Name: Arsalan Khan

Purpose: This is the solution to problem 1 given on the project euler website.
         The link to the particular problem can be found here https://projecteuler.net/problem=1
         
         The problem requires us to find the sum of the multiples of 3 & 5 within a given range.
         I will be comparing the speed of 2 different methods to see what is will have the fastest
         execution time. The upper limit will increase by a magnitude of 1 up to a maximum of 10,000
         the execution time will be averaged over 100 runs, and will be presented in microseconds. The
         results will be saved to a file called executionCompare.csv. Results will be provided below as well.
         
         Three methods are as follows:
         
          1) Using summation formula, requires 3 different loops, and requires addition
            1.1) Requires 2 additions, and one comapre for each loop
            
          2) Using the mod operator, requires only 1 loop, addition, and two extra compares
            2.2) Requires two additions and 3 compares for each loop
		 
Results: After running the tests, and creating a few simple graphs it was evident that method (1) was 
         quicker than method (2) by about an average of 10 microseconds. It should be noted that cpu time
         may not have been completly dedicated to program when testing, this caused some abnormal behaviour
         but the execution time was average over 100 runs to mitigate that. 

Last Updated: Jul 10, 2018
