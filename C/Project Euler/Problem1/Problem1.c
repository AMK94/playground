/**
Name: Arsalan Khan
Project: Multiples of 3 & 5, Problem #1
Probelm Link: https://projecteuler.net/problem=1

Purpose: Given a maximum number, the program will find the sum of all the multiples
         of 3 & 5 below said maximum number.
         
         I will also be assessing the timing differences between using the mod operator
         versus a simple summation. The results will be stored in a csv file called 
         peProblemOneStats
		 
The program should be given an input number when called. If it isn't a maximum number will
be requested when it is ran.
*/

#include <stdint.h>
#include <stdio.h>
#include <sys/time.h>

#define SECOND_MULTIPLIER 1##000##000
#define TEST_UPPER_LIMIT  10##000

// Set TEST to 1 if you would like to run test
// Set NO_TEST to 1 if you would like to run the regular program
#define TEST 1
#define NO_TEST 0

/**
Name: calcMulSumNoMod
Purpose: Return the sum of all multiples of 3 & 5 which are less than the maximum number provided
         This function will not make use of mod
                  
Input: (maxNum) - unsigned - upper limit of the sequence
Output: sum of all multiples of 3 & 5 less than maxNum
*/
unsigned calcMulSumNoMod (unsigned maxNum);

/**
Name: calcMulSumMod
Purpose: Return the sum of all multiples of 3 & 5 which are less than the maximum number provided
         This function will make use of mod
Input: maxNum - unsigned - upper limit of the sequence
Output: sum of all multiples of 3 & 5 less than maxNum
*/
unsigned calcMulSumMod (unsigned maxNum);


int main (uint8_t numArguments, char *inputNum[])
{
  FILE *csvWrite;
  struct timeval start, end, timeSpentNoMod = {0}, timeSpentMod = {0};
  unsigned sumNoMod, sumMod, maxNum;
  unsigned i;
  
  
#if TEST 
  /**
    The section given below is for testing purposes only.
  */
    
  csvWrite = fopen("executionCompare.csv", "w+");
  fprintf (csvWrite, "Upper Limit,Summation Time,Summation Result,Mod Operator Time,Mod Operator Result\n");
    
  for (maxNum = 10; maxNum < TEST_UPPER_LIMIT; maxNum++)
  {
    for (i = 0; i < 100; i++)
    {
      gettimeofday(&start, NULL);  
      sumNoMod = calcMulSumNoMod (maxNum);
      gettimeofday(&end, NULL);
      
      timeSpentNoMod.tv_usec += ((end.tv_sec - start.tv_sec) * SECOND_MULTIPLIER) + (end.tv_usec - start.tv_usec);
      
      
      gettimeofday(&start, NULL);
      sumMod = calcMulSumMod (maxNum);
      gettimeofday(&end, NULL);
      
      timeSpentMod.tv_usec += ((end.tv_sec - start.tv_sec) * SECOND_MULTIPLIER) + (end.tv_usec - start.tv_usec);
    }
    timeSpentNoMod.tv_usec = timeSpentNoMod.tv_usec/i;
    timeSpentMod.tv_usec = timeSpentMod.tv_usec/i;
    
    fprintf (csvWrite, "%u,%u,%u,", maxNum, timeSpentNoMod.tv_usec, sumNoMod);
    fprintf (csvWrite, "%u,%u\n", timeSpentMod.tv_usec, sumMod);
    
  }
  
  fclose(csvWrite); 
#elif NO_TEST
  if (numArguments == 2)
    sscanf(inputNum[1], "%u", &maxNum);
  else
  {
    // Proper format was not provided, ask for maximum number
    printf("Enter a maximum number: ");
    scanf("%u", &maxNum);
  }
  gettimeofday(&start, NULL);  
  sumNoMod = calcMulSumNoMod (maxNum);
  gettimeofday(&end, NULL);
  
  timeSpentNoMod.tv_usec = ((end.tv_sec - start.tv_sec) * SECOND_MULTIPLIER) + (end.tv_usec - start.tv_usec);
  printf ("Time taken without the mod operator : %u\n", timeSpentNoMod.tv_usec);
  
  printf("Sum with no mod operator: %u \n", sumNoMod);
  
  gettimeofday(&start, NULL);
  sumMod = calcMulSumMod (maxNum);
  gettimeofday(&end, NULL);
  
  timeSpentMod.tv_usec = ((end.tv_sec - start.tv_sec) * SECOND_MULTIPLIER) + (end.tv_usec - start.tv_usec);
  printf ("Time taken with the mod operator : %u\n", timeSpentMod.tv_usec);
  
  printf("Sum with mod operator: %u \n", sumMod); 
#endif
  printf("Complete\n");

  return 0;

}


unsigned calcMulSumNoMod (unsigned maxNum)
{
  unsigned i;
  unsigned sumThree = 0, sumFive = 0, sumRepeat = 0;  

  for (i = 0; (i) < maxNum; (i += 3))
    sumThree += i;
  
  for (i = 0; (i) < maxNum; (i += 5))
    sumFive += i;

  for (i = 0; (i) < maxNum; (i += 15))
    sumRepeat += i; 
  

  return (sumThree + sumFive - sumRepeat);
}

unsigned calcMulSumMod (unsigned maxNum)
{
  unsigned i, sum = 0;
  
  for (i = 0; i < maxNum; i++)
  {
    if ((i % 3 == 0) || (i % 5 == 0))
      sum += i;
  }
  
  return sum; 
}




