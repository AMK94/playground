Name: Arsalan Khan


Purpose: This program will take a text file which contains all words in the english language
         and construct a random password. Password will be constructed with 5 random words picked
         from the dictionary. 
         
Note: The dictionary provided was processed using grep commands and a python script to create the
      header file dictionary.h, this contains a struct of all the words parsed into a character array.
      I chose to put the words in a header file to make the program faster, instead of sorting through 
      the dictionary everytime the program is run.      
		 
Files: 
       passwordGen.c 
       dictionary.h 
       make

Last Updated: Jul 10, 2018
