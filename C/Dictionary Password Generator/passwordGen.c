/*
Name: Arsalan Khan
Project: Random Password Generator
Date: Jul 10, 2017

Purpose: This program will seed using the current time.
         It will then use that random value to go to a
         location in our dictionary. Once there it will read out one word.
         It will then generate a new random number, and print out the next,
         until all 5 words have been outputted.
*/

#include <time.h>
#include <stdio.h>
#include <stdlib.h>
#include <stdint.h>
#include "dictionary.h"

//Maximum characters in our array
#define MAXIMUM_ALLOWABLE_CHARACTERS 606279

extern char dictionary[MAXIMUM_ALLOWABLE_CHARACTERS];

int main()
{
	// declare a reference to my randomly generated number
	uint32_t randomGenNumber = 0;
	uint32_t *reference = &randomGenNumber;

	// Seed our random number generator then obtain a random value
	srand(time(0));
	randomNumGen (reference);
  
  
	//Loop to get five words, each time we will generate a new random number
	for (uint32_t i = 0; i < 5; i++)
	{
		randomWordGet(randomGenNumber);
		randomNumGen(reference);
	}
  
	// print newline for formatting at end
	printf("%c", '\n');
  system("pause");
}


void randomWordGet(uint32_t index)
{
	//Mod our random number by max allowed characters to asure we are in the
	//correct range
	index %= MAXIMUM_ALLOWABLE_CHARACTERS;
	//This will itterate through to make sure we are at the start of a word
	while (dictionary[index] != '\n')
	{
		index++;
	}
	//loop for printing current word
	while (dictionary[index+1] != '\n')
	{
		//If we happen to be on the last word, go to set the index to 0
		if (dictionary[index + 1] == '\0')
			index = 0;
		printf("%c", dictionary[index + 1]);
		index++;
	}
	// add a spacer for formatting
	printf("%c", ' ');
}

//assign a new random value to the randNum reference
void randomNumGen(uint32_t *randNum)
{
	*randNum = rand() * rand();
}


