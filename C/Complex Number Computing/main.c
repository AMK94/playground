#include <stdint.h>
#include <stdio.h>
#include <stdlib.h>
#include <ctype.h>
#include <string.h>
#include "complex.h"


#define MAXIMUM_PARAMS_ACCEPTED 5
#define MINIMUM_PARAMS_ACCEPTED 2
#define MAXIMUM_CHARACTERS_ACCEPTED 50


/**
Name: printValue
Input: two bool(uint8_t) values indicating what needs to be output, and a complex representing the result
Return: None
Purpose: The purpose of this function is to simply remove printing commands from the main body of the program.
		 The logic is fairly simple, we can either have a result that is a single floating point number, or another
		 which is a complex number. This function simply determines which, by using three checks.

		 Is the single result flag set?
		 Is the correctOperator flag set?
		 Is the imaginary portion of the complex number positive?
*/
void printValue(uint8_t correctOperator, uint8_t singleAnswer, struct Complex * result);

/**
Name: openingMessage
Input: None
Return: None
Purpose: This function will display a simple message to the user to assist
         with using the program. 
*/
void openingMessage (void);

/**
Name: getCommand
Input: 
      1) char* - This will have the user input stored in it
      2) struct Complex p1 - store the first complex number
      3) struct Complex p2 - store the second complex number
Return: None
Purpose: This function will accept an input from the user, format it
         to be all capital alphas, and store the complex numbers accordingly.
*/
void getCommand (char * userInput, struct Complex * p1, struct Complex * p2);

/**
Name: help
Input: None
Return: None
Purpose: Provide a help menu for user
*/
void help (void)
{
  printf ("\nThe list of valid commands is presented below in the following manner:\n");
  printf ("command  ---  format  ---  description\n\n");
  printf ("add  --- add 1 2 3 4 --- Adds two complexs pairs together\n");
  printf ("sub  --- sub 1 2 3 4 --- Subtracts two complexs pairs together\n");
  printf ("mul  --- mul 1 2 3 4 --- Multiplies two complexs pairs together\n");
  printf ("div  --- div 1 2 3 4 --- Divides two complexs pairs together\n");
  printf ("abs  --- abs 1 2     --- Gets the absolute value of a complex\n");
  printf ("exp  --- exp 1 2     --- Gets e^(complex)\n"); 
  printf ("inv  --- inv  1 2     --- Gets the inverse of the given complex\n"); 
  printf ("degr --- degr 1 2    --- Gets the angle of a complex in radians\n");
  printf ("degd --- degd 1 2    --- Gets the angle of a complex in degrees\n");  

}
int main ()
{
  char userInput[MAXIMUM_CHARACTERS_ACCEPTED];
  
  struct Complex p1 = {1 , 1}, p2 = {0 , 0}, result = {0 , 0};
  uint8_t singleAnswer, correctOperator;


  openingMessage();
  getCommand (userInput, &p1, &p2);

  while (userInput[0] != 'Q')
  {
    
    if (strcmp(userInput, "ADD") == 0 )
    {
      result = addComp(&p1, &p2);
    }
    else if (strcmp(userInput, "SUB") == 0)
    {
      result = subComp(&p1, &p2);
    }
    else if (strcmp(userInput, "MUL") == 0)
    {
      result = mulComp(&p1, &p2);
    }
    else if (strcmp(userInput, "DIV") == 0)
    {
      result = divComp(&p1, &p2);
    }
    else if (strcmp(userInput, "ABS") == 0)
    {
      singleAnswer = 1;
      result.real = absComp(&p1);
    }
    else if (strcmp(userInput, "DEGR") == 0)
    {
      singleAnswer = 1;
      result.real = angleCompRad(&p1);
    }
    else if (strcmp(userInput, "DEGD") == 0)
    {
      singleAnswer = 1;
      result.real = angleCompDeg(&p1);
    }
    else if (strcmp(userInput, "EXP") == 0)
    {
      result = expComp(&p1);
    }
    else if (strcmp(userInput, "INV") == 0)
    {
      result = invComp(&p1);
    }
    else if (strcmp(userInput, "HELP") == 0)
    {
      help ();
    }
    else
    {
      correctOperator = 0;
    }
    
    printValue(correctOperator, singleAnswer, &result);
    
    getCommand (userInput, &p1, &p2);
  }
  
  return 0;
}


void printValue(uint8_t correctOperator, uint8_t singleResult, struct Complex * result)
{

	if (!singleResult && correctOperator)
	{
    printf ("%f %fj\n", result->real, result->imaginary);
	}
	else if (singleResult && correctOperator)
	{
		printf ("%f\n", result->real);
	}
	else 
	{
		printf("Invalid Command\n");
    help ();
	}
}

void openingMessage (void)
{
  printf("To see what valid commands are available please enter help at any time\n");
  printf("To exit the program enter q!\n");
  return;
}

void getCommand (char * userInput, struct Complex * p1, struct Complex * p2)
{
  uint8_t i = 0, numArguments;
  char temp[MAXIMUM_CHARACTERS_ACCEPTED];
  float z,x,c,v;
  
  printf("\nPlease enter a command: ");
  
  fgets(temp, 100, stdin);  
  sscanf(temp, "%s %f %f %f %f ", userInput, &z, &x, &c, &v);
  p1->real = z;
  p1->imaginary = x;
  p2->real = c;
  p2->imaginary = v;
  
  while (userInput[i])
    userInput[i] = toupper(userInput[i++]);
}






