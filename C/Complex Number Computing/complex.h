/**
Name: Arsalan Khan

Purpose: Below are the function prototypes that will be used. It is a simple roadmap
         of how I will be proceeding for this project
*/

#ifndef COMPLEX_H
#define COMPLEX_H


struct Complex
{
  double real, imaginary; 
};

/**
Name: addComp 
Input: two struct Complex numbers (struct definition given in comp.h)
Output: one struct Complex number 
Purpose: The purpose of this function is to do an addition of any given two struct Complex numbers
*/
struct Complex addComp(struct Complex * p1, struct Complex * p2);

/**
Name: subComp
Input: two struct Complex numbers (struct definition given in comp.h)
Output: one struct Complex number
Purpose: The purpose of this function is to a subtraction of any given two struct Complex numbers.
*/
struct Complex subComp(struct Complex * p1, struct Complex * p2);

/**
Name: mulComp
Input: two struct Complex numbers (struct definition given in comp.h)
Output: one struct Complex number
Purpose: The purpose of this function is take two given inputs, subtract them and return the multiple.
		 The method of multiplying struct Complex numbers is very similar to the FOIL method used in math. 
		 That method is simply applied here to the real and imaginary parts.
*/
struct Complex mulComp(struct Complex * p1, struct Complex * p2);

/**
Name: divComp
Input: two struct Complex numbers (struct definition given in comp.h)
Output: one struct Complex number
Purpose: The purpose of this function is take two given inputs, subtract them and return the quotient.
		 The method of dividing struct Complex numbers is similar to multiplying them, expect with a few additional steps.
		 The additional step is to divide by the double of the inversed input2. So (I1 * I2)/2*I2
*/
struct Complex divComp(struct Complex * p1, struct Complex * p2);

/**
Name: absComp
Input: single struct Complex number (struct definition given in comp.h)
Output: doubleing point representation of the absolute value
Purpose: the purpose of this function is to return the absolute value of any given struct Complex number.
		 this can be obtained by taking the square root of the sum of squares in the struct Complex number.
		 |x + jy| = (x^2 + y^2)^1/2
*/
double absComp(struct Complex * p1);

/**
Name: angleCompRad 
Input: single struct Complex number (struct definition given in comp.h)
Output: doubleing point representation of the angle of a given struct Complex number in radians
Purpose: The purpose of this function is to return the angle of any given struct Complex number, represented as a radian.
		 We can determine the angle simply by taking the inverse tan of the real and imaginary values.
		 <(x + jy) = tan-1(y/x)
*/
double angleCompRad(struct Complex * p1);

/**
Name: angleCompDeg
Input: single struct Complex number (struct definition given in comp.h)
Output: doubleing point representation of the angle of a given struct Complex number in degrees
Purpose: The purpose of this function is to return the angle of any given struct Complex number, represented in degrees.
		 We can determine the angle simply by taking the inverse tan of the real and imaginary values.
		 <(x + jy) = tan-1(y/x) -> radian value, once we have this we can do a simple conversion to change it to degrees.
		 x rad * 180/pi = x deg
*/
double angleCompDeg(struct Complex * p1);

/**
Name: expComp
Input: single struct Complex number (struct defiction given in comp.h)
Output: single struct Complex number
Purpose: The purpose of this function is to calculate the exponential of a given struct Complex number.
		 e^(x+jy) = (e^x) * (cos(y) + jsin(y))
*/
struct Complex expComp(struct Complex * p1);

/**
Name: invComp
Input: singl;e struct Complex number (struct defintion given in comp.h)
Output: single struct Complex number
Purpose: the purpose of this function is to give the inverse of any given struct Complex number, 1/(z+jy).
		 the simplest way of achiving this is to divide 1 by whatever the struct Complex number is. 
*/
struct Complex invComp(struct Complex * p1);


#endif
