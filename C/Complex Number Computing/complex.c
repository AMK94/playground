#include "Complex.h"
#include "math.h"

struct Complex addComp(struct Complex * p1, struct Complex * p2)
{
  struct Complex result;
  
  result.real = p1->real + p2->real;
  result.imaginary = p1->imaginary + p2->imaginary;
  
  return result;
}

struct Complex subComp(struct Complex * p1, struct Complex * p2)
{
  struct Complex result;
  
  result.real = p1->real - p2->real;
  result.imaginary = p1->imaginary - p2->imaginary;
  
  return result;  
}

struct Complex mulComp(struct Complex * p1, struct Complex * p2)
{
  struct Complex result;
  
  result.real = ((p1->real * p2->real) - (p1->imaginary * p2->imaginary));
  result.imaginary = ((p1->real * p2->imaginary) + (p1->imaginary * p2->real));
  
  return result;
}

struct Complex divComp(struct Complex * p1, struct Complex * p2)
{
  struct Complex result;

	p2->imaginary = -p2->imaginary;

	result = mulComp(p1, p2);

	result.real = (result.real / ((p2->real * p2->real) + (p2->imaginary * p2->imaginary)));
	result.imaginary = (result.imaginary / ((p2->real * p2->real) + (p2->imaginary * p2->imaginary)));

	return result;
}

double absComp(struct Complex * p1)
{
  return sqrt(pow(p1->real, 2) + pow(p1->imaginary, 2));
}

double angleCompRad(struct Complex * p1)
{
  return atan(p1->imaginary / p1->real);
}

double angleCompDeg(struct Complex * p1)
{
  double temp;
  const static double RAD_PI = (180/3.14);

  
	temp = atan(p1->imaginary / p1->real);
	temp = temp * RAD_PI;
  
	return temp;
}

struct Complex expComp(struct Complex * p1)
{
	struct Complex result;
	double scalarMultiple;
  
	scalarMultiple = exp(p1->real);
  
	result.real = scalarMultiple * cos(p1->imaginary);
	result.imaginary = scalarMultiple * sin(p1->imaginary);
  
	return result;   
}

struct Complex invComp(struct Complex * p1)
{
  struct Complex temp = { 1, 0 };
	return divComp(&temp, p1);
}

