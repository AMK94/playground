Name: Arsalan Khan

Purpose: This program will perform a desired arithmetic operation on a given
         pair of complex numbers. The user will be able to perform the following
         operations: ADD, SUB, DIVIDE, MULTIPLY, ABSOLUTE VAL, RECTANGULAR -> ANGLE in radians,
         RECTANGULAR -> ANGLE in degrees, COMPLEX EXPONENT, and INVERSE.
                     
Contract: User must provide variables in the indicated format, there will be
          checks to asure that the right amount of information is provide 
          per command. However the order of the complex number must always be
          presented as (x + yj), but the user is not to enter j when inputting.
          I.E. (2 + 3j) + (1 + 4j) would be entered as "add 2 3 1 4"
		 
Files: 
      1) main.c
      2) complex.c
      3) complex.h
        
Last Updated: Jul 12, 2018