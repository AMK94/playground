Name: Arsalan Khan
Project Name: Stopwatch
Purpose: A simple GUI which acts as a stop watch. You will be able to control
         the watch using the provided buttons, or by pressing corresponding keys.

Colour Pallet is as follows:
* Dark purple: #28262C -- rgb(40, 38, 44)
* Light blue:  #6387BF -- rgb(99, 135, 191)
* Red:         #C75A57 -- rgb(199, 90, 87)
* Green:       #4DD0A7 -- rgb(77, 208, 167)
* White:       #FDFFFD -- rgb(253, 255, 253)          
         
Last Updated: Jul 12, 2018
