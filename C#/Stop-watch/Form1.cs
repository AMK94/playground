﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Stop_watch
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
        }

        bool move, timerOn = false;
        int moveX, moveY;

        int totalTimeCSeconds;
        int totalTimeSeconds;
        int totalTimeMinutes;
        int totalTimeHours;
        int lapTimeCSeconds;
        int lapTimeSeconds;
        int lapTimeMinutes;
        int lapTimeHours;
        int lapNumbers;
        int end;


        // Name: moveWindow
        // Purpose: updates current curors position values
        void moveWindow (MouseEventArgs e)
        {
            move = true;
            moveX = e.X;
            moveY = e.Y;
        }

        // Name: pictureBox1_MouseDown_1
        // Purpose: Enables the app to be moved
        private void pictureBox1_MouseDown_1(object sender, MouseEventArgs e)
        {
            moveWindow(e);
        }

        // Name: pictureBox1_MouseUp_1
        // Purpose: Stops the app from being moved
        private void pictureBox1_MouseUp_1(object sender, MouseEventArgs e)
        {
            move = false;
        }

        // Name: pictureBox1_MouseMove_1
        // Purpose: Allows the app to be moved around
        private void pictureBox1_MouseMove_1(object sender, MouseEventArgs e)
        {
            if (move == true)
                this.SetDesktopLocation(MousePosition.X - moveX, MousePosition.Y - moveY);
        }

        // Name: label1_MouseDown
        // Purpose: Enables the app to be moved
        private void label1_MouseDown(object sender, MouseEventArgs e)
        {
            moveWindow(e);
        }

        // Name: label1_MouseUp
        // Purpose: Stops the app from being moved
        private void label1_MouseUp(object sender, MouseEventArgs e)
        {
            move = false;
        }

        // Name: label1_MouseMove
        // Purpose: Allows the app to be moved around
        private void label1_MouseMove(object sender, MouseEventArgs e)
        {
            if (move == true)
                this.SetDesktopLocation(MousePosition.X - moveX, MousePosition.Y - moveY);
        }

        // Name: close_Click
        // Purpose: Close the application on click
        private void close_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        // Name: formateTimeString
        // Purpose: This function will format the current time determined by the tick function
        //          and format it to be properly displayed on the GUI
        private string formatTimeString(int hours, int minutes, int seconds, int cSeconds)
        {
            return (String.Format("{0:00}", hours) + ":" +
                 String.Format("{0:00}", minutes) + ":" +
                 String.Format("{0:00}", seconds) + "." +
                 String.Format("{0:00}", cSeconds));
        }

        // Name: clockTime_Tick
        // Purpose: This function will run every 10ms to update the clock display on the gui
        private void clockTimer_Tick(object sender, EventArgs e)
        {
            hms.Text = formatTimeString(totalTimeHours, totalTimeMinutes,
                                               totalTimeSeconds, totalTimeCSeconds);

            if (timerOn)
            {
                totalTimeCSeconds++;
                if (totalTimeCSeconds >= 100)
                {
                    totalTimeSeconds++;
                    totalTimeCSeconds = 0;
                }
                if (totalTimeSeconds >= 60)
                {
                    totalTimeMinutes++;
                    totalTimeSeconds = 0;
                }
                if (totalTimeMinutes >= 60)
                {
                    totalTimeHours++;
                    totalTimeMinutes = 0;
                }
            }
        }


        private void lapReset_Click(object sender, EventArgs e)
        {
            if (lapReset.Text == "Lap")
            {
                addLap();
            }
            else if (lapReset.Text == "Reset")
                resetTimer();
        }

        void addLap()
        {
            string lapTime, lapNumber;
            int cSecondFix;

            if ((totalTimeCSeconds - lapTimeCSeconds) < 0)
            {
                totalTimeSeconds--;
                cSecondFix = 100 - (lapTimeCSeconds - totalTimeCSeconds);
            }
            else
            {
                cSecondFix = totalTimeCSeconds - lapTimeCSeconds;
            }

 
            lapNumbers++;
            lapNumber = "Lap " + String.Format("{0:00}", lapNumbers) + ": ";
                
            lapTime = formatTimeString(totalTimeHours - lapTimeHours, totalTimeMinutes - lapTimeMinutes,
                                             totalTimeSeconds - lapTimeSeconds, cSecondFix);
            lapTimes.Items.Add(lapTime);
            lap.Items.Add(lapNumber);

            lapTimeCSeconds = totalTimeCSeconds;
            lapTimeSeconds = totalTimeSeconds;
            lapTimeMinutes = totalTimeMinutes;
            lapTimeHours = totalTimeHours;

        }

        private void resetTimer()
        {
            totalTimeCSeconds = 0;
            totalTimeSeconds = 0;
            totalTimeMinutes = 0;
            totalTimeHours = 0;
            lapTimeCSeconds = 0;
            lapTimeSeconds = 0;
            lapTimeMinutes = 0;
            lapTimeHours = 0;
            lapNumbers = 0;

            lapTimes.Items.Clear();
            lap.Items.Clear();
        }

        // Name: startStop_Click
        // Purpose: This function will toggle the text for the startStop button, and the lap/reset button
        private void startStop_Click(object sender, EventArgs e)
        {
            timerOn = !timerOn;
            if (timerOn)
            {
                startStop.Text = "Stop";
                startStop.BackColor = Color.FromArgb(199, 90, 87);
                lapReset.Text = "Lap";
                lapReset.BackColor = Color.FromArgb(77, 208, 167);
            }
            else
            {
                if (lapNumbers == 0 )
                    defaultTime();
                startStop.Text = "Start";
                startStop.BackColor = Color.FromArgb(77, 208, 167);
                lapReset.Text = "Reset";
                lapReset.BackColor = Color.FromArgb(199, 90, 87);
            }
            
        }

        void defaultTime ()
        {
            string lapNumber, lapTime;
            lap.Items.Clear();
            lapTimes.Items.Clear();
            lapNumber = "Lap " + String.Format("{0:00}", 1) + ": ";

            lapTime = formatTimeString(totalTimeHours, totalTimeMinutes,
                                             totalTimeSeconds, totalTimeCSeconds);
            lapTimes.Items.Add(lapTime);
            lap.Items.Add(lapNumber);

        }

        private void minimize_Click(object sender, EventArgs e)
        {
            this.WindowState = FormWindowState.Minimized;
        }




    }
}
