Name: Arsalan Khan
Date Started: July 31, 2018
Date Finished: Aug 01, 2018

Project Name: Shopping Checkout System

How to compile:
Compiled using MinGW g++ compiler
g++ main.cpp ShopItem.cpp ShopCart.cpp -o output

The original problem statement can be found in the pdf named problemStatement".
My assumptions will be listed below as well as how I interpreted the problem.

/////////////////////////////
// Problem Statement
/////////////////////////////

Accept user inputs from a predefined shopping list, and calculate the total
cost of all items entered. Each item will have a unit price (ex 1 apple for 40 cents)
and some will have special pricing as well (ex 3 apples for 90 cents). 

/////////////////////////////
// Requirements/Constraints 
/////////////////////////////

1) The user must be able to adjust pricing information before the checkout begins.
2) The items to be added must be entered with no spaces (ex "banana", "apple", etc)
3) User input can be case-insensative
4) User will be allowed to enter items until they indicate "total"

/////////////////////////////
// User Conditions
/////////////////////////////
The user must provide input as indicated by the program. Basic error checking
has been added, but all cases are not accounted for. Refer to the help menu for
all valid commands.

No negative values should be used.

/////////////////////////////
// User Guide
/////////////////////////////
While using the program, the user will have six global commands available:
1) back : returns to the previous section
2) main : returns to the main menu 
3) show : displays the current inventory list, and if you have added any items 
          to the cart
4) help : displays list of all available commands          
5) total: totals the current items in the shopping cart
6) quit : ends the program


Below is a list of all valid commands, they will be formatted as follows:
format  --- purpose --- locality (i.e. show  --- displays current inventory -- global)

1)  back                 --- return to previous section                --- global
2)  main                 --- return to main menu                       --- global
3)  show                 --- displays current inventory                --- global
4)  quit                 --- terminates the program                    --- global
5)  help                 --- displays list of all commands             --- global
6)  total                --- changes to total state                    --- global
6a) finish               --- ends program                              --- totalState
6b) adjust               --- goes back to checkout state               --- totalState
6c) restart              --- destorys all changes and starts from init --- totalState
7)  additem              --- changes to add item state                 --- mainMenu
7a) add apple 0.4 3 0.9  --- adds to inventory                         --- addItem
    |-> (itemName itemPrice itemDiscountQuantity itemDiscountPrice)
    |->  1 apple = $0.40, 3 apples for $0.90
7b) edit apple 0.4 3 1.0 --- edits existing item price                 --- addItem
    |-> (itemName itemPrice itemDiscountQuantity itemDiscountPrice)
    |->  1 apple = $0.50, 3 apples = $1.0 now
7c) remove apple         --- removes indicated item from inventory     --- addItem
    |-> simply removes item completly from inventory
8)  checkout             --- changes to checkout state                 --- mainMenu
8a) add apple            --- adds one apple to the shopping cart       --- checkout
8b) remove apple         --- removes one apple from the shopping cart  --- checkout

/////////////////////////////
// Additions
/////////////////////////////

These will only be included if time permits

1) Allow user to remove items during checkout phase
2) Allow user to add new items to the menu

/////////////////////////////
// Methodology
/////////////////////////////

This program utilizes an Item class which contains the following variables:
- string itemName;
- string special;
- double itemPrice;
- double itemDiscount;
- unsigned discountQuantity;
- unsigned quantity;

This class is friends with another class which acts as a Finite State Machine.

the FSM class includes a vector<ShopItem> property, which allows easy addititions
and modifications of items. Below are the states utilized in the program:

1) invalidInput
2) mainMenu
3) showItems
4) editExistingItem
5) removeExistingItem
6) addNewItem
7) checkoutMenu
8) help
9) total
10)restart
11)quit

For explination of the states and how they interact, please refer to the blockDiagram pdf.


/////////////////////////////
// Version History:
/////////////////////////////
0.0
===============================================================================
Added basic readme, original problem prompt, & simple block diagram.
Will begin coding Aug 01, 2018

Files:
readme.txt
problemStatement.pdf
blockDiagram.png

Date: July 31, 2018
-------------------------------------------------------------------------------

1.0
===============================================================================
- Editted Block diagram to include data structures to be used (this will likely
  change again). 
- Completed ShopItem class
- Finished base for FSM class
- Added simple print functions to make output look clean

The program will currently initalize the shopping cart with the item and prices
given in the problem statement, it will also be able to print them in a coherent
manner. 

Files:
readme.txt
problemStatement.pdf
blockDiagram.png

main.cpp
ShopItem.cpp
ShopCart.cpp

ShopItem.h
ShopCart.h 

Date Aug 01, 2018
-------------------------------------------------------------------------------

2.0
===============================================================================
- Finished FSM class
- Established state switching logic
- Completed add/remove/edit inventory functions
- Completed checkout functions
- Completed total functions

The program is currently fully operational, please refer to the user guide above
on how to navigate the menus. 

I will now be doing some code revision and cleaning up unneeded code for future
versions

Files:
readme.txt
problemStatement.pdf
blockDiagram.png

main.cpp
ShopItem.cpp
ShopCart.cpp

ShopItem.h
ShopCart.h 
main.h

Date Aug 01, 2018
-------------------------------------------------------------------------------


2.1
===============================================================================
Test and completed for basic operation.

The program could still use better input checking, and thought-out do while function.


Files:
./textOutput
readme.txt
problemStatement.pdf
blockDiagram.png

main.cpp
ShopItem.cpp
ShopCart.cpp

ShopItem.h
ShopCart.h 
main.h

Date Aug 01, 2018
-------------------------------------------------------------------------------

2.2
===============================================================================
Updated the block diagram to be more correct, fixed up all spelling errors and
phrasing. Ready to submit

Files:
./textOutput
readme.txt
problemStatement.pdf
blockDiagram.png

main.cpp
ShopItem.cpp
ShopCart.cpp

ShopItem.h
ShopCart.h 
main.h

Date Aug 02, 2018
-------------------------------------------------------------------------------