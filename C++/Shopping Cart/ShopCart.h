#ifndef SHOPPINGCART_H
#define SHOPPINGCART_H

#include <iostream>
#include <iomanip>
#include <vector>
#include <string>
#include <fstream>

#include "ShopItem.h"

using namespace std;

/*
* Allowable states for the program. For more information about each state,
* please refer to the pdf provided with the source code.
*/
enum State {
  invalidInput,
  mainMenu,
  showItems,
  editExistingItem,
  removeExistingItem,
  addNewItem,
  checkoutMenu,
  helpState,
  total,
  restart,
  quit
};

const static unsigned DECIMAL_POINTS = 2;
const static unsigned SHORT_SPACING = 15;
const static unsigned LONG_SPACING = 20;
const static int ITEM_NOT_FOUND = -1;

class ShopCartFSM {
public:
  // Default Constructor
  ShopCartFSM();
  // Default Destructor
  ~ShopCartFSM();

  /*
  * @name initialCreate
  * @purpose to create an inventory/shopping cart of default values as indicated
  *          by the problem statement
  */
  void initialCreate ();

  // these functions are not used, therefore I am commenting them out
  // vector<ShopItem> getCart () const;
  // void setCart (vector<ShopItem> inventory);
  // void setNextState (State state);
  // State getNextState () const;

  // Accessor Functions
  State getCurrentState () const;
  // Mutator Functions
  void setCurrentState (State state);


  ////////////////////////////////////
  // State Logic/Functions & inputHandlers
  ////////////////////////////////////
  /*
  * @name determineState
  * @purpose Used to determine what state the user is moving to
  * @param string : string representation of the state
  */
  void determineState (string userInput);

  void determineBack (State state);
  /*
  * @name checkFAR
  * @purpose checks if the user input was back, quit, main, show, help, total
  * @param none
  * @returns true if user input was one of the checked patterns
  */
  bool checkBQMSHT (string userInput);

  /*
  * @name checkFAR
  * @purpose checks if the user input was finish, adjust, or restart.
  * @param none
  * @returns true if user input was one of the checked patterns
  */
  bool checkFAR (string userInput);

  /*
  * @name show
  * @purpose prints the current shopping cart
  * @param none
  */
  void show ();

  /*
  * @name help
  * @purpose prints a help message for valid commands for the user
  * @param none
  */
  void help();

  /*
  * @name addItemToInventory
  * @purpose holds the state logic for the add item state. Allows the user to
  *          add a new item to the cart.
  * @param none
  */
  void addItemToInventory ();

  /*
  * @name removeItemFromInventory
  * @purpose holds the state logic for the remove item state. Allows the user to
  *          remove an existing item in the cart.
  * @param none
  */
  void removeItemFromInventory ();

  /*
  * @name editItemInInventory
  * @purpose holds the state logic for the edit item state. Allows the user to
  *          edit an existing item in the cart.
  * @param none
  */
  void editItemInInventory ();

  /*
  * @name checkout
  * @purpose holds the state logic for the checkout state. Allows the user to
  *          add or remove items from their shopping cart.
  * @param none
  */
  void checkout ();

  /*
  * @name totalState
  * @purpose holds the state logic for the total state. Presents total cost of
  *          shopping cart, and presents options for continuing or ending.
  * @param none
  */
  void totalState ();

  /*
  * @name restartState
  * @purpose holds the state logic for the restart state
  * @param none
  */
  void restartState ();

  /*
  * @name calculateTotal
  * @purpose itterates through the shopping cart, and sums all the item costs
  * @return double : total summed value of the shopping cart
  * @param none
  */
  double calculateTotal ();

  ////////////////////////////////////
  // Vector / Itterator Functions
  ////////////////////////////////////
  /*
  * @name findItem
  * @purpose indicates if a item is within our inventory or notValid
  * @note this function returns an index if the item is found. If not
  *       then the function will return -1.
  * @parama string : the item to be found
  */
  int findItem (string userInput);

  ////////////////////////////////////
  // Output Functions
  ////////////////////////////////////

  /*
  * @name printItems
  * @purpose to take a given vector of ShopItems, and print all items within the
  *          vector
  * @param vector<ShopItem>& : Reference to the object that will be printed
  */
  void printItems();

  /*
  * @name printTable
  * @purpose to provide an easy way of formatting data entries to look tabulated
  * @param T : This is the value being printed
  * @param spacing : The width the parameter requires
  * @param ostream : The output stream to display on
  */
  template<typename T> void printTable(T t, const int& spacing);

  /*
  * @name printMain
  * @purpose provides the main instructions for the user
  * @param None
  */
  void printMain ();

  /*
  * @name printFile
  * @purpose to print text to the screen, without cluttering the code
  * @note this function depends on the text files in the textOutput Directory
  *       if a text file goes missing, it must be replaced.
  * @param string : location of file to be printed
  */
  void printFile (string fileLocation);

private:
  ////////////////////////////////////
  // State Logic / Functions
  ////////////////////////////////////
  State currentState;
  State nextState;

  ////////////////////////////////////
  // Shopping cart of items
  ////////////////////////////////////
  vector<ShopItem> cart;

  // Instead of putting defines for strings globally
  // I will declare a char* to mimic a string.
  // Since i will only be comparing equality, this should pose no issues
  static constexpr const char* QUIT = "quit";
  static constexpr const char* MAIN_MENU = "main";
  static constexpr const char* SHOW = "show";
  static constexpr const char* BACK = "back";
  static constexpr const char* HELP = "help";
  static constexpr const char* ADD_ITEM = "addinventory";
  static constexpr const char* EDIT_ITEM = "editinventory";
  static constexpr const char* REMOVE_ITEM = "removeinventory";
  static constexpr const char* CHECKOUT = "checkout";
  static constexpr const char* ADD_CART = "add";
  static constexpr const char* REMOVE_CART = "remove";
  static constexpr const char* TOTAL = "total";
  static constexpr const char* RESTART = "restart";
  static constexpr const char* ADJUST = "adjust";
  static constexpr const char* FINISH = "finish";

  // The following are file paths for help files, so there aren't magic strings
  // in the code
  static constexpr const char* welcome = "./textOutput/welcome.txt";
  static constexpr const char* welcomeHelp = "./textOutput/welcomeHelp.txt";
  static constexpr const char* totalHelp = "./textOutput/totalHelp.txt";
  static constexpr const char* checkoutHelp = "./textOutput/checkoutHelp.txt";
  static constexpr const char* editItemHelp= "./textOutput/editItemHelp.txt";
  static constexpr const char* removeItemHelp = "./textOutput/removeItemHelp.txt";
  static constexpr const char* addItemHelp = "./textOutput/addItemHelp.txt";
  static constexpr const char* commandHelp = "./textOutput/help.txt";
  static constexpr const char* changeMade = "./textOutput/changeMade.txt";
};

#endif
