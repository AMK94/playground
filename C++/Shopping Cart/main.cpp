#include <iostream>
#include <vector>
#include <iomanip>

#include "ShopCart.h"
#include "ShopItem.h"
#include "main.h"

using namespace std;

int main () {

  ShopCartFSM shopping;
  string userInput;

  shopping.printMain();


  do {
    switch(shopping.getCurrentState()) {
      case quit:
        cout << "Thanks for shopping with us!\n";
        break;
      case showItems:
        shopping.show();
        break;
      case helpState:
        shopping.help();
        break;
      case mainMenu:
        cout << "\nMain Menu : ";
        cin >> userInput;
        lowerString(userInput);
        shopping.determineState(userInput);
        break;
      case addNewItem:
        shopping.addItemToInventory();
        break;
      case editExistingItem:
        shopping.editItemInInventory();
        break;
      case removeExistingItem:
        shopping.removeItemFromInventory();
        break;
      case checkoutMenu:
        shopping.checkout();
        break;
      case total:
        shopping.totalState();
        break;
      case restart:
        shopping.restartState();
        break;
      case invalidInput:
        cout << "Not a valid command\n";
        shopping.setCurrentState(mainMenu);
        break;
    }
  } while (shopping.getCurrentState() != quit);

  return 0;

}

void lowerString (string &input) {
  unsigned size = input.size();
  for (unsigned i = 0; i < size; i++)
    input[i] = tolower(input[i]);
}
