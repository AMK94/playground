#include "ShopItem.h"
#include "main.h"
ShopItem::ShopItem() {
  // Do not need to set string, because it is set to null on initailzation
  itemPrice = 0;
  itemDiscount = 0;
  discountQuantity = 0;
  quantity = 0;
}

ShopItem::ShopItem(string name, double price, double discount,
                   unsigned discountQ) {
  // Format the name to always be lower case, regardless of user input
  lowerString (name);

  itemName = name;
  itemPrice = price;
  quantity = 0;

  if (discountQ == 0) {
    discountQuantity = 1;
    itemDiscount = 0;
    special = "No Special";
  }
  else {
    itemDiscount = (price * discountQ) - discount;
    discountQuantity = discountQ;

    // Below is code for simple to format the string for representing sales
    specialString (special, price, itemDiscount, discountQuantity);
  }

}

ShopItem::~ShopItem() {}

string ShopItem::getItemName() const {
  return itemName;
}

string ShopItem::getItemSpecial() const {
  return special;
}

double ShopItem::getItemPrice() const {
  return itemPrice;
}

double ShopItem::getItemDiscount() const {
  return itemDiscount;
}

unsigned ShopItem::getDiscountQuantity() const {
  return discountQuantity;
}

unsigned ShopItem::getQuantity() const {
  return quantity;
}

void ShopItem::setItemName(string name) {
  itemName = name;
}

void ShopItem::setItemSpecial (string spec) {
  special = spec;
}

void ShopItem::setItemPrice(double price) {
  itemPrice = price;
}

void ShopItem::setItemDiscount (double discount) {
  itemDiscount = discount;
}

void ShopItem::setDiscountQuantity (unsigned discountQ) {
  discountQuantity = discountQ;
}

void ShopItem::setQuantity (unsigned amount) {
  quantity = amount;
}

void ShopItem::specialString (string &special, double price, double itemDiscount
                              , unsigned discountQuantity) {
  double specialPrice = (price * discountQuantity) - itemDiscount;
  special = to_string(discountQuantity) + " for $" + to_string(specialPrice);
  // to_string leaves a lot of trailing zeros, below is a simple fix
  special.erase ( special.find_last_not_of('0') + 1, string::npos );
  special.erase ( special.find_last_not_of('.') + 1, string::npos );
}

void ShopItem::editItemInformation (double newPrice, double newDiscountPrice,
                                    unsigned newDiscountQ) {
  string specialPricing;
  setItemPrice(newPrice);
  setDiscountQuantity (newDiscountQ);
  setItemDiscount(newDiscountPrice);
  specialString (specialPricing, newPrice, newDiscountPrice,newDiscountQ);
  setItemSpecial(specialPricing);
}
