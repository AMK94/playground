#include "ShopCart.h"
#include "main.h"

ShopCartFSM::ShopCartFSM () {
  initialCreate();
}

ShopCartFSM::~ShopCartFSM () {}

// This function was created so that the restart functionality would work without
// extra code
void ShopCartFSM::initialCreate () {
  // This is the default items required by the problem statement
  ShopItem item1 ("aPple", 0.40, 0.9, 3);
  ShopItem item2 ("bAnana", 0.75, 6.00, 10);
  ShopItem item3 ("coconut", 2.00, 3.00, 2);
  ShopItem item4 ("donut", 0.50, 0, 0);

  cart.push_back(item1);
  cart.push_back(item2);
  cart.push_back(item3);
  cart.push_back(item4);

  currentState = mainMenu;
}

// Unused Functions
// vector<ShopItem> ShopCartFSM::getCart() const {
//   return cart;
// }
// void ShopCartFSM::setCart (vector<ShopItem> inventory) {
//   cart = inventory;
// }
// State ShopCartFSM::getNextState () const {
//   return nextState;
// }

//
// void ShopCartFSM::setNextState (State state) {
//   nextState = state;
// }

State ShopCartFSM::getCurrentState () const {
  return currentState;
}

void ShopCartFSM::setCurrentState (State state) {
  currentState = state;
}

void ShopCartFSM::determineState (string userInput) {
  if (userInput == QUIT) {
    currentState = quit;
  }
  else if (userInput == MAIN_MENU) {
    currentState = mainMenu;
  }
  else if (userInput == SHOW) {
    nextState = currentState;
    currentState = showItems;
  }
  else if (userInput == BACK) {
    determineBack(currentState);
    nextState = mainMenu;
  }
  else if (userInput == HELP) {
    nextState = currentState;
    currentState = helpState;
  }
  else if (userInput == ADD_ITEM) {
    currentState = addNewItem;
  }
  else if (userInput == EDIT_ITEM) {
    currentState = editExistingItem;
  }
  else if (userInput == REMOVE_ITEM) {
    currentState = removeExistingItem;
  }
  else if (userInput == CHECKOUT) {
    currentState = checkoutMenu;
  }
  else if (userInput == TOTAL) {
    currentState = total;
  }
  else if (userInput == RESTART) {
    currentState = restart;
  }
  else {
    currentState = invalidInput;
  }
}

void ShopCartFSM::determineBack (State state) {
  if (state == mainMenu) {
    currentState = mainMenu;
  }
  else if (state == editExistingItem) {
    currentState = mainMenu;
  }
  else if (state == addNewItem) {
    currentState = mainMenu;
  }
  else if (state == removeExistingItem) {
    currentState = mainMenu;
  }
  else if (state == checkoutMenu) {
    currentState = mainMenu;
  }
  else if (state == total) {
    currentState = checkoutMenu;
  }
}

bool ShopCartFSM::checkBQMSHT (string userInput) {
  if (userInput == BACK) {
    determineBack(currentState);
    return true;
  }
  else if (userInput == QUIT) {
    determineState (QUIT);
    return true;
  }
  else if (userInput == MAIN_MENU) {
    determineState (MAIN_MENU);
    return true;
  }
  else if (userInput == SHOW) {
    determineState (SHOW);
    return true;
  }
  else if (userInput == HELP) {
    determineState (HELP);
    return true;
  }
  else if (userInput == TOTAL) {
    determineState (TOTAL);
    return true;
  }
  return false;
}

bool ShopCartFSM::checkFAR (string userInput) {
  if (userInput == FINISH) {
    determineState (QUIT);
    return true;
  }
  else if (userInput == ADJUST) {
    determineState (CHECKOUT);
    return true;
  }
  else if (userInput == RESTART) {
    determineState (RESTART);
    return true;
  }
  return false;
}

void ShopCartFSM::show () {
  printItems ();
  currentState = nextState;
}

void ShopCartFSM::help() {
  printFile (commandHelp);
  currentState = nextState;
}

void ShopCartFSM::addItemToInventory () {
  string userInput;
  double newPrice, newDiscountPrice;
  unsigned newDiscountQ;
  int itemIndex = ITEM_NOT_FOUND;

  printFile(addItemHelp);

  do {
    cin >> userInput;
    if (checkBQMSHT(userInput)) {
      return;
    }
    cin >> newPrice >> newDiscountQ >> newDiscountPrice;
    itemIndex = findItem (userInput);
    if (itemIndex != ITEM_NOT_FOUND) {
      cout << "Item already exists! Go to the main menu to edit it!\n\n";
      cout << "Add Item : ";
    }
  } while (itemIndex != ITEM_NOT_FOUND);

  ShopItem temp(userInput, newPrice, newDiscountPrice, newDiscountQ);
  cart.push_back(temp);
  printFile (changeMade);

  determineState (MAIN_MENU);
}

void ShopCartFSM::removeItemFromInventory () {
  string userInput;
  double newPrice, newDiscountPrice;
  unsigned newDiscountQ;
  int itemIndex = ITEM_NOT_FOUND;

  printFile (removeItemHelp);

  do {
    cin >> userInput;
    if (checkBQMSHT(userInput)) {
      return;
    }
    itemIndex = findItem (userInput);
    if (itemIndex == ITEM_NOT_FOUND) {
      cout << "Item does not exist, cannot remove\n\n";
      cout << "Remove Item KhanMart: ";
    }
  } while (itemIndex == ITEM_NOT_FOUND);
  cart.erase(cart.begin() + itemIndex);
  printFile (changeMade);

  determineState (MAIN_MENU);
}

void ShopCartFSM::editItemInInventory () {
  string userInput;
  double newPrice, newDiscountPrice;
  unsigned newDiscountQ;
  int itemIndex = ITEM_NOT_FOUND;

  printFile (editItemHelp);

  do {
    cin >> userInput;
    if (checkBQMSHT(userInput)) {
      return;
    }
    cin >> newPrice >> newDiscountQ >> newDiscountPrice;
    itemIndex = findItem (userInput);
    if (itemIndex == ITEM_NOT_FOUND)
      cout << "Item does not exist\n\nEdit Item: ";
  } while (itemIndex == ITEM_NOT_FOUND);

  newDiscountPrice = (newPrice * newDiscountQ) - newDiscountPrice;
  cart[itemIndex].editItemInformation(newPrice, newDiscountPrice, newDiscountQ);
  printFile(changeMade);

  determineState (MAIN_MENU);
}

void ShopCartFSM::checkout() {
  string addRemove, name;
  int itemIndex;
  printFile (checkoutHelp);

  do {
    cin >> addRemove;
    if (checkBQMSHT(addRemove)) {
      return;
    }
    cin >> name;
    itemIndex = findItem (name);
    if (itemIndex == ITEM_NOT_FOUND)
      cout << "Item does not exist or invalid input\nCheckout : ";
  } while (itemIndex == ITEM_NOT_FOUND);

  if (addRemove == ADD_CART)
    cart[itemIndex].setQuantity(cart[itemIndex].getQuantity() + 1);
  else if (addRemove == REMOVE_CART) {
    if (cart[itemIndex].getQuantity() == 0)
      cout << "This item is not currently in the shopping cart!\n\n";
    else
      cart[itemIndex].setQuantity(cart[itemIndex].getQuantity() - 1);
  }
}

void ShopCartFSM::totalState() {

  string userInput;
  bool notValid = true;

  cout << "The current total is: " << calculateTotal() << endl << endl;

  printFile (totalHelp);

  do {
    cin >> userInput;
    if (checkBQMSHT (userInput)) {
      return;
    }
    notValid = !checkFAR (userInput);
    if (notValid) {
      cout << "Invalid command\nFinish KhanMart : ";
    }

  } while (notValid);
}

void ShopCartFSM::restartState () {
  cart.erase(cart.begin(), cart.end());
  initialCreate();
  determineState(MAIN_MENU);
}

double ShopCartFSM::calculateTotal () {
  unsigned size = cart.size();
  double totalCost = 0;

  for (unsigned i = 0; i < size; i++) {
    // total += (price * quantity) - ((quantity/dicountQ) * discount price)
    totalCost += ((cart[i].getItemPrice() * cart[i].getQuantity()) -
            ((cart[i].getQuantity()/cart[i].getDiscountQuantity()) *
              cart[i].getItemDiscount()));
  }
  return totalCost;
}

int ShopCartFSM::findItem (string userInput) {
  unsigned size = cart.size();

  for (unsigned i = 0; i < size; i++) {
    if (cart[i] == userInput) {
      return i; // if item is found, return index
    }
  }
  return ITEM_NOT_FOUND; // If item is not found, return -1
}

void ShopCartFSM::printItems() {
  unsigned size = cart.size();

  printTable ("Item Name", SHORT_SPACING);
  printTable ("Unit Price", SHORT_SPACING);
  printTable ("Special", LONG_SPACING);
  printTable ("Quantity", SHORT_SPACING);
  cout << endl;

  for (unsigned i = 0; i < size; i++) {
    cout << fixed << setprecision(DECIMAL_POINTS);
    printTable(cart[i].getItemName(), SHORT_SPACING);
    printTable(cart[i].getItemPrice(), SHORT_SPACING);
    printTable(cart[i].getItemSpecial(), LONG_SPACING);
    printTable (cart[i].getQuantity(), SHORT_SPACING);
    cout << endl;
  }
}

template<typename T> void ShopCartFSM::printTable(T t, const int& spacing) {
  cout << left << setw(spacing) << setfill(' ') << t;
}

void ShopCartFSM::printMain () {
  printFile (welcome);
  printItems ();
  printFile (welcomeHelp);

}

void ShopCartFSM::printFile (string fileLocation) {
  string line;
  ifstream helpFile(fileLocation);

  if (helpFile.is_open()) {
    while (getline (helpFile, line)) {
      cout << line;
      if (!helpFile.eof()) {
        cout << endl;
      }
    }
    helpFile.close();
  }
  else {
    cout << "Unable to open help file, please check textOutput Directory\n";
  }
}
