#include <iostream>
#include <iomanip>
#include <fstream>

#include "main.h"
#include "ShopItem.h"
#include "ShopCart.h"
#include "StateMachine.h"



int main () {

  char fileName[CHAR_ARRAY_LIMIT] = "./cartInitFile/create.txt";
  ShopCart * shoppingCart = new ShopCart (fileName);
  StateMachine FSM;

  printFile (welcome);
  shoppingCart->printCart ();
  printFile (welcomeHelp);

  do {
    switch (FSM.getState()) {
      case mainState:
        FSM.mainMenu ();
        break;
      case helpState:
        FSM.printHelp ();
        break;
      case showState:
        FSM.showCart (*shoppingCart);
        break;
      case backState:
        FSM.moveBackState();
        break;
      case totalState:
        FSM.totalCart(*shoppingCart);
        break;
      case quitState:
        cout << "Thanks for shopping with us!\n";
        break;
      case editInvState:
        FSM.editInventoryState (*shoppingCart);
        break;
      case addInvState:
        FSM.addInventoryState (*shoppingCart);
        break;
      case removeInvState:
        FSM.removeInvenotryState(*shoppingCart);
        break;
      case checkoutState:
        FSM.checkoutCart(*shoppingCart);
        break;
      case restartState:
        cout << "Wiping all changes from default...\n";
        delete shoppingCart;
        shoppingCart = new ShopCart (fileName);
        FSM.transitionState (mainState);
        break;
      case invalidState:
        cout << "Invalid Command\n";
        FSM.transitionState(FSM.moveBackState());
    }
    cout << endl;
  } while (FSM.getState () != quitState);

  return 0;
}

void lowerString (string &input) {
  unsigned size = input.size();
  for (unsigned i = 0; i < size; i++)
    input[i] = tolower(input[i]);
}


void printFile (string fileLocation) {
  string line;
  ifstream helpFile(fileLocation);

  if (helpFile.is_open()) {
    while (getline (helpFile, line)) {
      cout << line;
      if (!helpFile.eof()) {
        cout << endl;
      }
    }
    helpFile.close();
  }
  else {
    cout << "Unable to open help file, please check textOutput Directory\n";
  }
}

template<typename T>
void printTable(const T t, const int spacing) {
  cout << left << setw(spacing) << setfill(' ') << t;
}
