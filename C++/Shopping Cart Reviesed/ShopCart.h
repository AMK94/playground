#ifndef SHOPCART_H
#define SHOPCART_H

#include <string>
#include <vector>

#include "ShopItem.h"
using namespace std;

class ShopCart {

public:
  /*
  * @name ShopCart
  * @purpose This function is the overload constructor, it will take a text file
             which should be pre-formatted to create the inital shopping cart.
  * @important The init file must be in the correct format. It can be a multi
               line file but the contents must be in the order of:
               itemName - unitPrice - discountQuantity - discountPrice
               if this format is not met the initial cart will be incorrect.
  * @param fileName : Name of the file to be used
  */
  ShopCart (const char * fileName);

  // default destructor
  ~ShopCart ();

  /*
  * @name printCart
  * @purpose This function will print the contents of the shopping cart
  * @param none
  * @return none
  */
  void printCart ();

  /*
  * @name printTable
  * @purpose to provide an easy way of formatting data entries to look tabulated
  * @param T : This is the value being printed
  * @param spacing : The width the parameter requires
  * @param ostream : The output stream to display on
  */
  template<typename T> void printTable(T t, const int& spacing);

  /*
  * @name editItemInInventory
  * @purpose This function will edit an existing inventory item with updated
              parameters
  * @param item : Name of the item to edit
  * @param price : New price of the item
  * @param discountQ : The new discount quantity
  * @param discountPrice : The new discount price
  * @return none
  */
  void editItemInInventory (string item, double price, unsigned discountQ,
                            double discountPrice);

  /*
  * @name addItemToInventory
  * @purpose This function will append in item to the shopping cart
  * @param ShopItem : Item to be added to the shopping cart
  * @return none
  */
  void addItemToInventory (ShopItem item);

  /*
  * @name removeItemFromInventory
  * @purpose This function will remove the given item from the shopping cart
  * @param item : Item to be removed
  * @return none
  */
  void removeItemFromInventory (string item);

  /*
  * @name addItemToCart
  * @purpose This function will increment the quantity variable of a cart item
  * @param item : Item to be incremented
  * @return none
  */
  void addItemToCart (string item);

  /*
  * @name removeItemFromCart
  * @purpose This function will decrement the quantity variable of a cart item
  * @param item : Item to be decremented
  * @return none
  */
  void removeItemFromCart (string item);

  /*
  * @name calculateCartTotal
  * @purpose This function will return the dollar value of the shopping cart
  * @param none
  * @return double : Total $ value of the shopping cart
  */
  double calculateCartTotal ();


  /*
  * @name findItem
  * @purpose This function will return the index of an item in the shopping cart
             if the item does not exist it will return a -1.
  * @param item : Item to be found
  * @return int : index of the item
  */
  int findItem (string item);


private:
  // a vector of ShopItems to mimic a shopping cart
  vector<ShopItem> cart;
};



#endif
