#include <iostream>
#include <iomanip>
#include <fstream>

#include "ShopCart.h"
#include "main.h"


ShopCart::ShopCart (const char * fileName) {
  char name[CHAR_ARRAY_LIMIT];
  double price, discount;
  unsigned discountQ;
  FILE * initFile;

  initFile = fopen(fileName, "r");

  while (fscanf ( initFile, "%s %lf %u %lf", name, &price, &discountQ, &discount) == 4) {
      ShopItem item (name, price, discountQ, discount);
      cart.push_back(item);
  }
  fclose(initFile);
}

ShopCart::~ShopCart () {}


void ShopCart::printCart () {
  unsigned size = cart.size();

  printTable ("Item Name", SHORT_SPACING);
  printTable ("Unit Price", SHORT_SPACING);
  printTable ("Special", LONG_SPACING);
  printTable ("Quantity", SHORT_SPACING);
  cout << endl;

  for (unsigned i = 0; i < size; i++) {
    cout << fixed << setprecision(DECIMAL_POINTS);
    printTable(cart[i].getItemName(), SHORT_SPACING);
    printTable(cart[i].getItemPrice(), SHORT_SPACING);
    printTable(cart[i].getItemSpecial(), LONG_SPACING);
    printTable (cart[i].getQuantity(), SHORT_SPACING);
    cout << endl;
  }
}


template<typename T> void ShopCart::printTable(T t, const int& spacing) {
  cout << left << setw(spacing) << setfill(' ') << t;
}

void ShopCart::editItemInInventory (string item, double price, unsigned discountQ,
                          double discountPrice) {
  int newDiscountPrice;
  int index = findItem (item);

  if (index == ITEM_NOT_FOUND) {
    cout << "Item does not exist, cannot edit\n";
  }
  else {
    cart[index].editItemInformation (price, discountPrice, discountQ);
  }
}

void ShopCart::addItemToInventory (ShopItem item) {
  cart.push_back (item);
}

void ShopCart::removeItemFromInventory (string item) {
  int index = findItem (item);

  if (index == ITEM_NOT_FOUND) {
    cout << "Item does not exist, cannot remove\n";
  }
  else {
    cart.erase (cart.begin() + index);
  }

}


void ShopCart::addItemToCart (string item) {
  int index = findItem (item);

  if (index == ITEM_NOT_FOUND) {
    cout << "Item does not exist, cannot add to cart\n";
  }
  else {
    cart[index].setQuantity(cart[index].getQuantity() + 1);
  }
}

void ShopCart::removeItemFromCart (string item) {
  int index = findItem (item);

  if (index == ITEM_NOT_FOUND) {
    cout << "Item does not exist, cannot remove from cart\n";
  }
  else {
    if (cart[index].getQuantity() != 0) {
      cart[index].setQuantity(cart[index].getQuantity() - 1);
    }
  }
}

double ShopCart::calculateCartTotal () {
  unsigned size = cart.size();
  double totalCost = 0, regularCost, discountAdjust;
  unsigned discountsApplied = 0;

  for (unsigned i = 0; i < size; i++) {
    // total = price*quantity - (quantity/discountQ) * (discountPrice - unit price)
    discountsApplied = cart[i].getQuantity()/cart[i].getDiscountQuantity();
    regularCost = cart[i].getQuantity () * cart[i].getItemPrice ();
    discountAdjust = (regularCost - cart[i].getItemDiscount ()) * discountsApplied;

    totalCost += (regularCost - discountAdjust);
  }
  return totalCost;
}

int ShopCart::findItem (string userInput) {
  unsigned size = cart.size();
  for (unsigned i = 0; i < size; i++) {
    if (cart[i].getItemName () == userInput) {
      return i; // if item is found, return index
    }
  }
  return ITEM_NOT_FOUND; // If item is not found, return -1
}
