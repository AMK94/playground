/*
File Name: StateMachine.h
Purpose: Declaration for the state machine used in the Shopping
         cart program.

*/
#ifndef STATEMACHINE_H
#define STATEMACHINE_H

#include "ShopCart.h"

// List of the states that are possible in the FSM
// all function lists, abide by the order these states are shown in
enum states {
  mainState,
  helpState,
  showState,
  backState,
  totalState,
  quitState,
  editInvState,
  addInvState,
  removeInvState,
  checkoutState,
  restartState,
  invalidState = 0xff
};

class StateMachine {
public:
  // default constructor for the StateMachine, sets currentState to mainState
  // and declares help & back booleans as false
  StateMachine ();

  // default destructor
  ~StateMachine ();

  // Simple function to determine the current state of the FSM
  states getState ();

  // Simple function to set the current state of the FSM
  void setState (states state);

  /*
  * @name getUserInput
  * @purpose This function will get user input in a different manner depending
             on the state of the FSM, it will always return a ShopItem object,
             but it will only have a value if it is required by the state.
             Otherwise it will simply accept regular user commands
  * @param userInput : a string passed by reference to store the user command
  * @param current : an enum of the current state the FSM is in
  * @return ShopItem : a ShopItem object indicating user input
  */
  ShopItem getUserInput (string & userInput, states current);

  /*
  * @name transitionState
  * @purpose This function will perform a state transition as indicated by the
             user. It will take into account if the transition is caused by a
             back command or, normal user command. It will append to the
             previous state vector.
  * @param userInput : an enum indicating the state the user is switching to
  * @return none
  */
  void transitionState (states userInput);

  /*
  * @name moveBackState
  * @purpose This function will return the previous state visited by the user
             and remove it from the previous state vector
  * param none
  * @return states : a representation of the state to go back to
  */
  states moveBackState ();

  /*
  * @name checkValidCommand
  * @purpose Given a string user command this function will determine what state
             to transition, and return it
  * @param userInput : a string of the user command
  * @return states : the state which the FSM will change to
  */
  states checkValidCommand (string userInput);

  /*
  * @name checkGlobalCommand
  * @purpose This function will check if one of the six global commands were
             entered by the user
  * @param userInput
  * @return bool : true if a global command was entered.
  */
  bool checkGlobalCommand (string userInput);

  /*
  * @name displayHelp
  * @purpose This function will display the help parameters for different states
  * @param fileName : The file location of the help information
  * @return none
  */
  void displayHelp (string fileName);

  /*
  * @name mainMenu
  * @purpose This function will accept user input, and transition states from
             the main menu. This is the mainState function
  * @param none
  * @return none
  */
  void mainMenu ();

  /*
  * @name printHelp
  * @purpose This function will print the pre-made help text file, which
             contains the command help instructions. This is the helpState
             function
  * @param none
  * @return none
  */
  void printHelp ();

  /*
  * @name showCart
  * @purpose This function will print the contents of the ShopCart object passed
             and then transition states. This is the showState function
  * @param ShopCart : The cart to be printed
  * @return none
  */
  void showCart (ShopCart & cart);

  /*
  * @name editInventoryState
  * @purpose This function will accept userInput for an item to edit. User
             will be locked into this state unless a global command is used.
             A state transition will occur on a valid global command. This is
             the editInvState function.
  * @param ShopCart : Cart which contains items to be edited
  * @return none
  */
  void editInventoryState (ShopCart & cart);

  /*
  * @name addInventoryState
  * @purpose This function will accept userInput for an item to add. User
             will be locked into this state unless a global command is used.
             A state transition will occur on a valid global command. This is
             the addInvState function.
  * @param ShopCart : Cart which item will be added to
  * @return none
  */
  void addInventoryState (ShopCart & cart);

  /*
  * @name removeInvenotryState
  * @purpose This function will accept userInput for an item to remove. User
             will be locked into this state unless a global command is used.
             A state transition will occur on a valid global command. This is
             the removeInvState function.
  * @param ShopCart : Cart which item will be removed from
  * @return none
  */
  void removeInvenotryState (ShopCart & cart);

  /*
  * @name checkoutCart
  * @purpose This function will accept user input to add/remove item quantities
             from the existing list of items. User will be locked into this
             state unless a global command is used. This is the checkoutState
             function.
  * @param ShopCart : Cart which will increment/decrement item quantites
  * @return none
  */
  void checkoutCart (ShopCart & cart);

  /*
  * @name totalCart
  * @purpose This function will display the current price total of the shopping
             cart and provide the user with options to adjust the cart, restart
             the shopping experience, or finish shopping.
  * @param ShopCart : The cart to determine the total price of
  * @return none
  */
  void totalCart (ShopCart & cart);


private:
  // trakcer for current FSM state
  states currentState;
  // vector of states to keep track of state history, incase user wants to back
  vector<states> previousStates;

  // boolean values to help determine if information should be displayed
  bool backInvoked;
  bool helpDisplayed;

  // Instead of putting defines for strings globally
  // I will declare a char* to mimic a string.
  // Since i will only be comparing equality, this should pose no issues
  static constexpr const char* QUIT = "quit";
  static constexpr const char* MAIN_MENU = "main";
  static constexpr const char* SHOW = "show";
  static constexpr const char* BACK = "back";
  static constexpr const char* HELP = "help";
  static constexpr const char* ADD_ITEM = "addinventory";
  static constexpr const char* EDIT_ITEM = "editinventory";
  static constexpr const char* REMOVE_ITEM = "removeinventory";
  static constexpr const char* CHECKOUT = "checkout";
  static constexpr const char* ADD_CART = "add";
  static constexpr const char* REMOVE_CART = "remove";
  static constexpr const char* TOTAL = "total";
  static constexpr const char* RESTART = "restart";
  static constexpr const char* ADJUST = "adjust";
  static constexpr const char* FINISH = "finish";


};

#endif
