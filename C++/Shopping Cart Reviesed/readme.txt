Name: Arsalan Khan
Date Started: Aug 07, 2018
Date Finished:

Project Name: Shopping Checkout System Revised

The original problem statement can be found in the pdf named problemStatement".
My assumptions will be listed below as well as how I interpreted the problem.

This is a revised version of an existing solution. Here are the changes that will
be made:

1) Dedicated FSM
2) Better state transitions
3) Better class connections and structure
4) Simpler layout

/////////////////////////////
// Problem Statement
/////////////////////////////

Accept user inputs from a predefined shopping list, and calculate the total
cost of all items entered. Each item will have a unit price (ex 1 apple for 40 cents)
and some will have special pricing as well (ex 3 apples for 90 cents).

/////////////////////////////
// Requirements/Constraints
/////////////////////////////

1) The user must be able to adjust pricing information before the checkout begins.
2) The items to be added must be entered with no spaces (ex "banana", "apple", etc)
3) User input can be case-insensitive
4) User will be allowed to enter items until they indicate "total"

/////////////////////////////
// User Conditions
/////////////////////////////
The user must provide input as indicated by the program. Basic error checking
has been added, but all cases are not accounted for. Refer to the help menu for
all valid commands.

No negative values should be used.


/////////////////////////////
// User Guide
/////////////////////////////
While using the program, the user will have six global commands available:
1) back : returns to the previous section
2) main : returns to the main menu 
3) show : displays the current inventory list, and if you have added any items 
          to the cart
4) help : displays list of all available commands          
5) total: totals the current items in the shopping cart
6) quit : ends the program

Below is a list of all valid commands, they will be formatted as follows:
format  --- purpose --- locality (i.e. show  --- displays current inventory -- global)

1)   back                 --- return to previous section                --- global
2)   main                 --- return to main menu                       --- global
3)   show                 --- displays current inventory/cart quantity  --- global
4)   quit                 --- terminates the program                    --- global
5)   help                 --- displays list of all commands             --- global
6)   total                --- displays current item total               --- global
6a)  finish               --- ends program                              --- totalState
6b)  adjust               --- goes back to checkout                     --- totalState
6c)  restart              --- destorys all changes and starts from init --- totalState
7)   addInventory         --- allows user to enter item to be added     --- mainMenu
7a)  apple 0.4 3 0.9      --- adds to inventory                         --- addCart
     |-> (itemName itemPrice itemDiscountQuantity itemDiscountPrice)
     |->  1 apple = $0.40, 3 apples for $0.90
8)   editInventory        --- allows user to enter item to be edited    --- mainMenu
8a)  apple 0.4 3 1.0      --- edits existing item price                 --- editCart
     |-> (itemName itemPrice itemDiscountQuantity itemDiscountPrice)
     |->  1 apple = $0.50, 3 apples = $1.0 now
9)   removeInventory      --- allows user to enter item to be removed   --- mainMenu
9a)  apple                --- removes indicated item from inventory     --- removeCart
     |-> simply removes item completly from inventory
10)  checkout             --- changes to checkout state                 --- mainMenu
10a) add apple            --- adds one apple to the shopping cart       --- checkout
10b) remove apple         --- removes one apple from the shopping cart  --- checkout


/////////////////////////////
// Version History
/////////////////////////////

1.0
--------------------------------------------------------------------------------
Copied over the ShopItem class and made a few changes.
Created new ShopCart class with new functions.

Will work on the FSM for the next revision.
Time: 1 hour

2.0
--------------------------------------------------------------------------------
Finished up FSM functions.
- Back functionality working
- edit/add/remove items from inventory working
- show/help working
- add/remove cart working
- total working
- restart state working

Just need to add in comments and beautify the program a little bit.

Time: 160 minutes

3.0
--------------------------------------------------------------------------------
Finished commenting all the header files, fixed small issue with invalid state.
Updated readme. This is now compelte, until i find a bug.

Time: 80 minutes


