Below is a list of all valid commands, they will be formatted as follows:
format  --- purpose --- locality (i.e. show  --- displays current inventory -- global)

1)   back                 --- return to previous section                --- global
2)   main                 --- return to main menu                       --- global
3)   show                 --- displays current inventory/cart quantity  --- global
4)   quit                 --- terminates the program                    --- global
5)   help                 --- displays list of all commands             --- global
6)   total                --- displays current item total               --- global
6a)  finish               --- ends program                              --- totalState
6b)  adjust               --- goes back to checkout                     --- totalState
6c)  restart              --- destorys all changes and starts from init --- totalState
7)   addInventory         --- allows user to enter item to be added     --- mainMenu
7a)  apple 0.4 3 0.9      --- adds to inventory                         --- addCart
     |-> (itemName itemPrice itemDiscountQuantity itemDiscountPrice)
     |->  1 apple = $0.40, 3 apples for $0.90
8)   editInventory        --- allows user to enter item to be edited    --- mainMenu
8a)  apple 0.4 3 1.0      --- edits existing item price                 --- editCart
     |-> (itemName itemPrice itemDiscountQuantity itemDiscountPrice)
     |->  1 apple = $0.50, 3 apples = $1.0 now
9)   removeInventory      --- allows user to enter item to be removed   --- mainMenu
9a)  apple                --- removes indicated item from inventory     --- removeCart
     |-> simply removes item completly from inventory
10)  checkout             --- changes to checkout state                 --- mainMenu
10a) add apple            --- adds one apple to the shopping cart       --- checkout
10b) remove apple         --- removes one apple from the shopping cart  --- checkout

