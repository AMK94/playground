#include "StateMachine.h"

#include <iostream>
#include "main.h"
using namespace std;

StateMachine::StateMachine () {
  // Only initalize current state, since there are no past states yet
  currentState = mainState;
  backInvoked = false;
  helpDisplayed = false;
}

StateMachine::~StateMachine () {}

states StateMachine::getState () {
  return currentState;
}

ShopItem StateMachine::getUserInput (string & userInput, states current) {

  string command = ""; // will also double as itemName if adding/editting inv
  string itemName;
  double price, discountPrice;
  unsigned discountQ;
  ShopItem item;
  states transition;

  cin >> command;
  userInput = command;
  item.setItemName(command);
  if (checkGlobalCommand (command)) {
    userInput = command;
    lowerString (userInput);
    return item;
  }

  if (current == addInvState || current == editInvState) {
    cin >> price >> discountQ >> discountPrice;
    item.setItemPrice(price);
    item.setDiscountQuantity (discountQ);
    item.setItemDiscount (discountPrice);
    item.specialString (discountPrice, discountQ);
  }
  else if (current == checkoutState) {
    cin >> itemName;
    item.setItemName (itemName);
  }
  lowerString (userInput);

  return item;
}

void StateMachine::transitionState (states userInput) {
  helpDisplayed = false;

  if (!backInvoked) {
    previousStates.push_back(currentState);
  }
  else if (backInvoked) {
    backInvoked = false;
  }
  currentState = userInput;
}

states StateMachine::moveBackState () {
  states temp;
  if (!previousStates.empty()) {
    temp = previousStates.back();
    previousStates.pop_back();
    backInvoked = true;
  }
  else {
    temp = mainState;
  }
  return temp;
}

states StateMachine::checkValidCommand (string userInput) {
  if (userInput == MAIN_MENU) {
    return mainState;
  }
  else if (userInput == HELP) {
    return helpState;
  }
  else if (userInput == SHOW) {
    return showState;
  }
  else if (userInput == BACK) {
    return moveBackState();
  }
  else if (userInput == TOTAL) {
    return totalState;
  }
  else if (userInput == QUIT) {
    return quitState;
  }
  else if (userInput == EDIT_ITEM && currentState == mainState) {
    return editInvState;
  }
  else if (userInput == ADD_ITEM && currentState == mainState) {
    return addInvState;
  }
  else if (userInput == REMOVE_ITEM && currentState == mainState) {
    return removeInvState;
  }
  else if (userInput == CHECKOUT && currentState == mainState) {
    return checkoutState;
  }
  else if (userInput == ADD_CART && currentState == checkoutState) {
    return checkoutState;
  }
  else if (userInput == REMOVE_CART && currentState == checkoutState) {
    return checkoutState;
  }
  else if (userInput == RESTART && currentState == totalState) {
    return restartState;
  }
  else if (userInput == ADJUST && currentState == totalState) {
    return checkoutState;
  }
  else if (userInput == FINISH && currentState == totalState) {
    return quitState;
  }
  return invalidState;
}

bool StateMachine::checkGlobalCommand (string userInput) {
  if (userInput == MAIN_MENU) {
    return true;
  }
  else if (userInput == HELP) {
    return true;
  }
  else if (userInput == SHOW) {
    return true;
  }
  else if (userInput == BACK) {
    return true;
  }
  else if (userInput == TOTAL) {
    return true;
  }
  else if (userInput == QUIT) {
    return true;
  }
  return false;
}

void StateMachine::displayHelp (string fileName) {
  if (!helpDisplayed) {
    printFile (fileName);
    helpDisplayed = true;
  }
}

void StateMachine::mainMenu () {
  string userInput;
  states userState;

  cout << "main menu: ";

  getUserInput (userInput, currentState);
  userState = checkValidCommand (userInput);
  transitionState (userState);

}

void StateMachine::printHelp () {
  printFile (commandHelp);
  transitionState (moveBackState ());
}

void StateMachine::showCart (ShopCart &cart) {
  cart.printCart ();
  transitionState (moveBackState ());
}

void StateMachine::editInventoryState (ShopCart & cart) {
  string userInput = "";
  ShopItem item;
  states transition;

  displayHelp (editItemHelp);
  cout << "edit item: ";
  item = getUserInput (userInput, currentState);

  if (checkGlobalCommand (userInput)) {
    transitionState (checkValidCommand (userInput));
    return;
  }

  if (cart.findItem (item.getItemName()) != ITEM_NOT_FOUND) {
    cart.editItemInInventory (item.getItemName(), item.getItemPrice(),
                              item.getDiscountQuantity(), item.getItemDiscount());
    cout << "changes made\n";
  }
  else {
    cout << "Item does not exist or invalid command\n";
  }
}

void StateMachine::addInventoryState (ShopCart & cart) {
  string userInput = "";
  ShopItem item;
  states transition;

  displayHelp (addItemHelp);
  cout << "add item: ";
  item = getUserInput (userInput, currentState);

  if (checkGlobalCommand (userInput)) {
    transitionState (checkValidCommand (userInput));
    return;
  }

  if (cart.findItem (item.getItemName()) == ITEM_NOT_FOUND) {
    cart.addItemToInventory (item);
    cout << "changes made\n";
  }
  else {
    cout << "Item already exists or invalid command\n";
  }
}

void StateMachine::removeInvenotryState (ShopCart & cart) {
  string userInput = "";
  ShopItem item;
  states transition;

  displayHelp (removeItemHelp);
  cout << "remove item: ";
  item = getUserInput (userInput, currentState);

  if (checkGlobalCommand (userInput)) {
    transitionState (checkValidCommand (userInput));
    return;
  }

  if (cart.findItem (item.getItemName()) != ITEM_NOT_FOUND) {
    cart.removeItemFromInventory (item.getItemName());
    cout << "changes made\n";
  }
  else {
    cout << "Item does not exist or Invalid Command\n";
  }
}

void StateMachine::checkoutCart (ShopCart & cart) {
  string userInput = "";
  ShopItem item;
  states transition;

  displayHelp (checkoutHelp);
  cout << "checkout: ";
  item = getUserInput (userInput, currentState);

  if (checkGlobalCommand (userInput)) {
    transitionState (checkValidCommand (userInput));
    return;
  }

  if (cart.findItem (item.getItemName ()) != ITEM_NOT_FOUND) {
    if (userInput == ADD_CART) {
      cart.addItemToCart(item.getItemName ());
    }
    else if (userInput == REMOVE_CART) {
      cart.removeItemFromCart(item.getItemName ());
    }
  }
  else {
    cout << "Item does not exist or Invalid Command\n";
  }
}

void StateMachine::totalCart (ShopCart & cart) {
  string userInput;
  states temp;

  cout << "The current total is: " << cart.calculateCartTotal () << "\n\n";
  displayHelp (totalHelp);
  cout << "Total: ";
  getUserInput (userInput, currentState);

  transitionState (checkValidCommand (userInput));
}
