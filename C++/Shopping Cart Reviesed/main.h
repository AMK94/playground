#ifndef MAIN_H
#define MAIN_H

#include <iostream>
#include <fstream>
#include <stdio.h>

using namespace std;

const static int CHAR_ARRAY_LIMIT = 100;
// simple declerations to avoid using magic numbers
const static unsigned DECIMAL_POINTS = 2;
const static unsigned SHORT_SPACING = 15;
const static unsigned LONG_SPACING = 20;
const static int ITEM_NOT_FOUND = -1;

// The following are file paths for help files, so there aren't magic strings
// in the code
static constexpr const char* welcome = "./textOutput/welcome.txt";
static constexpr const char* welcomeHelp = "./textOutput/welcomeHelp.txt";
static constexpr const char* totalHelp = "./textOutput/totalHelp.txt";
static constexpr const char* checkoutHelp = "./textOutput/checkoutHelp.txt";
static constexpr const char* editItemHelp= "./textOutput/editItemHelp.txt";
static constexpr const char* removeItemHelp = "./textOutput/removeItemHelp.txt";
static constexpr const char* addItemHelp = "./textOutput/addItemHelp.txt";
static constexpr const char* commandHelp = "./textOutput/help.txt";
static constexpr const char* changeMade = "./textOutput/changeMade.txt";



/*
* @name lowerString
* @purpose to convert a given string to all lower because
* @param string& : refernece to the string that will change
*/
void lowerString (string &input);

/*
* @name printFile
* @purpose to print text to the screen, without cluttering the code
* @note this function depends on the text files in the textOutput Directory
*       if a text file goes missing, it must be replaced.
* @param string : location of file to be printed
*/
void printFile (string fileLocation);


#endif
