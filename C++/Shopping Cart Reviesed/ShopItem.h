#ifndef SHOPITEM_H
#define SHOPITEM_H


#include <string>
using namespace std;

class ShopItem {
public:

  // default constructor, sets all fields to null/0
  ShopItem ();

  /* Overload Constructor
  * The quantity variable is not included in the call because when the user
  * creates a new item, it will always have 0 quantity. No need to ask the user
  * to enter an extra number.
  */
  ShopItem(string name, double price,unsigned discountQ,
           double discount);

  // Default Destructor
  ~ShopItem();

  /* Accessor Functions
  * List of simple accessor for the class
  * getItemName: Retrieves the objects itemName property
  * getItemPrice: Retrieves the objects itemPrice property
  * getItemDiscount: Retrieves the objects itemDiscount property
  * getDiscountQuantity: Retrieves the objects discountQuantity property
  * getQuantity: Retrieves the objects quantity property
  */
  string getItemName() const;
  string getItemSpecial() const;
  double getItemPrice() const;
  double getItemDiscount() const;
  unsigned getDiscountQuantity() const;
  unsigned getQuantity() const;

  /* Mutator Functions
  * List of simple mutators for the class
  * setItemName: Modifies the objects itemName property
  * setItemPrice: Modifies the objects itemPrice property
  * setItemDiscount: Modifies the objects itemDiscount property
  * setDiscountQuantity: Modifies the objects discountQuantity property
  * setQuantity: Modifies the objects quantity property
  */
  void setItemName(string name);
  void setItemSpecial(string special);
  void setItemPrice(double price);
  void setItemDiscount (double discount);
  void setDiscountQuantity (unsigned discountQ);
  void setQuantity (unsigned amount);

  /*
  * @name specialString
  * @purpose create a string for the item special, with given parameters
  * @param string& : string reference of sale string
  * @param double : price of the item
  * @param double : the price of a batch of items
  * @parama unsigned : amount of items needed to apply discount
  */
  void specialString (double itemDiscount, unsigned discountQuantity);
  /*
  * @name editItemInformation
  * @purpose edit information for an existing shop item
  * @param double : new price of the item
  * @param double :new price deduction for the discounted item
  * @parama unsigned : new amount of items needed to apply discount
  */
  void editItemInformation (double newPrice, double newDiscountPrice,
                            unsigned newDiscountQ);


private:
  /* Member Variables
  * itemName: Holds the string value of a purchasable item
  * itemPrice: Holds the unit price of the item
  * itemDiscount: Holds the discount amount for the item
  *   |-> if one apple is 40c, and three apples are 90c
  *   |-> then the itemDiscount will be 30 = (40c * discountQuantity) - 90c
  * dicountQuantity: The amount repeated items needed to apply a discountQuantity
  * quantity: The current number of items in the cart
  */
  string itemName;
  string special;
  double itemPrice;
  double itemDiscount;
  unsigned discountQuantity;
  unsigned quantity;
};


#endif
