#Name: Frequency Sort


##Problem: Given an array of integers with n items in it, sort the array first by ascending frequency, then magnitude. 
         
         
I.E if you are given an array a[n] = { 3, 1, 2, 4, 2, 1 } after it has been sorted it should look like the following


aSorted[n] = { 3, 4, 1, 1, 2, 2}


The program will allow the user to enter a custom array, and the program will sort it with the afformentioned rules.
The user must follow the constraints listed below.


1. 1 <= a[n] <= 1,000,000
2. 1 <= n <= 200,000


If the above is obeyed, the program will do as follows.


1. The program will sort all items first by ascending frequency, then ascending magnitude
2. The program will output the sorted array to the stdout with each element seperate by a newline


Last Updated: Jul 13, 2018
