#include <iostream>
#include <vector>
#include <algorithm> 

using namespace std;

/**
Name: customSort
Input: unSorted -> A unsorted vector of integers
Output: string output to stdout of the sorted vector
Return: None
Purpose: This function will take any give array which meets the constraints of
         1 <= a[n] <= 1,000,000  && 1 <= n <= 200,000 and sort it first by 
         ascending frequency, then by magnitude.

*/
void customSort (vector<int>& arraySort);

int main ()
{
  int arraySize, i;
  
  cout << "Please enter the size of the array: ";
  cin >> arraySize;
  
  vector<int> arraySort (arraySize, 0);
  
  cout << "Please enter the array contents, seperate by space or enter: \n";
  
  for (i = 0; i < arraySize; i++)
    cin >> arraySort[i];

  customSort (arraySort);
  
  cout << "The sorted vector is:\n";
  for (int i = 0; i < arraySize; i++)
       cout << arraySort[i] << endl;
  
  return 0;
}


void customSort (vector<int>& arraySort)
{
  
  vector<int>::iterator low;
  int arraySize = arraySort.size(); 
  int maxSize = *max_element(arraySort.begin(), arraySort.end());
  int position = 1, i = 0, j, temp;
  
  vector<int> middleMan (maxSize, 0);
    
  // we are simply tallying up the frequency of each number occurance here
  for (int i = 0; i < arraySize; i++)
    middleMan[arraySort[i] - 1]++;
  
  // clear the array to be re-sorted
  arraySort.clear();
    
  // This do while loop will itterate through all the possible repetitions
  // while saving the lowest of each in the sorted vector, until it is all sorted.
  do 
  {
    // First we find if the lowest frequency of exists. This starts at 1, and is incremented
    // until no more of that frequency is found.
    low = find (middleMan.begin(), middleMan.end(), position);
    
    // Once found the index of that number is put appended to the vector
    for (j = 0; j < middleMan[low - middleMan.begin()]; j++)
      arraySort.push_back(low - middleMan.begin() + 1);
    // The position is then set to 0, so that it is not picked up again
    middleMan[low-middleMan.begin()] = 0;
    
    // Once we can no longer find the frequency number we are looking for, increase it
    if (low == middleMan.end())
      position++;

    // continue until we have reached the maximum array size.
  } while ((position <= arraySize));
}  

  
  
  
  
  
  
  
  
  