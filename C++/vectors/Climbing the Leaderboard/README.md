#Name: Climbing the Leaderboard


##Problem: Refer to the following website for the problem prompt https://www.hackerrank.com/challenges/climbing-the-leaderboard/problem
           
      
The program will take the input of two given arrays, the first which will be an existing leaderboard, and the second the scores an individual
has obtained over their plays. The output will describe how the individual compares to the leaderboard after each play. 

For the full problem description please refer to the link provide above.


Note: Passed all test cases. It should be noted that the array of scores for alice were increasing based on magnitude in all of the given test cases.
A few individuals had solutions that worked if Alices' score array increase with each increasing index, however would fail if it was a lower number.
This is because they simply decremented the max index until a match was found, and then moved to the next score. My program takes into account the existance
of an unordered Alice score array, this must be done because the problem statement does not indicate that it will be ordered. 


## Problem Success Rate: 60.36%, for 33241 submissions.


Last Updated: Jul 18, 2018
