#include <bits/stdc++.h>
#include <algorithm> 
using namespace std;

vector<string> split_string(string);


// Complete the climbingLeaderboard function below.
vector<int> climbingLeaderboard(vector<int> scores, vector<int> alice) {
 

  int leaderboardSize = scores.size();
  int aliceSize = alice.size();
  int condensedBoardIndex = 0;
  int previousVal = 0;
  int i;
  int compare;
  
  vector<int> condensedBoard (leaderboardSize);
  vector<int> result (aliceSize);
  alice.push_back(2000000000);
  // create a board with no repeating scores
  for (i = 0; i < leaderboardSize; i++)
  {
    previousVal = scores[i];
    if ( i == leaderboardSize)
      condensedBoard[condensedBoardIndex++] = previousVal;
    else if (previousVal != scores[i+1])
    {
      condensedBoard[condensedBoardIndex++] = previousVal;
    }
  }

  // trim extra 0's off the end of the condensed board
  for ( i = condensedBoardIndex; i < leaderboardSize; i++)
    condensedBoard.pop_back();
  
  leaderboardSize = condensedBoardIndex;

  
  for (i = 0; i < aliceSize; i++)
  {
    while (alice[i] >= condensedBoard[condensedBoardIndex - 1])
    {
      condensedBoardIndex--;
      if (condensedBoardIndex == 0)
        break;
    }
    result[i] = condensedBoardIndex + 1;
    if (alice[i] > alice[i+1])
    {
      condensedBoardIndex = leaderboardSize;
    }
  }
    
  return result;
}

int main()
{

    int scores_count;
    
    cout << "Enter the number of scores on the leader board: ";
    cin >> scores_count;
    cin.ignore(numeric_limits<streamsize>::max(), '\n');

    string scores_temp_temp;
    
    cout << "Enter the scores seperated by spaces: ";
    getline(cin, scores_temp_temp);

    vector<string> scores_temp = split_string(scores_temp_temp);

    vector<int> scores(scores_count);

    for (int i = 0; i < scores_count; i++) {
        int scores_item = stoi(scores_temp[i]);

        scores[i] = scores_item;
    }

    int alice_count;
    
    cout << "Enter the amount of times Alice played: ";
    cin >> alice_count;
    cin.ignore(numeric_limits<streamsize>::max(), '\n');

    string alice_temp_temp;
    cout << "Enter alices scores: "; 
    getline(cin, alice_temp_temp);

    vector<string> alice_temp = split_string(alice_temp_temp);

    vector<int> alice(alice_count);

    for (int i = 0; i < alice_count; i++) {
        int alice_item = stoi(alice_temp[i]);

        alice[i] = alice_item;
    }

    vector<int> result = climbingLeaderboard(scores, alice);

    for (int i = 0; i < result.size(); i++) {
        cout << result[i];

        if (i != result.size() - 1) {
            cout << "\n";
        }
    }

    cout << "\n";


    return 0;
}

vector<string> split_string(string input_string) {
    string::iterator new_end = unique(input_string.begin(), input_string.end(), [] (const char &x, const char &y) {
        return x == y and x == ' ';
    });

    input_string.erase(new_end, input_string.end());

    while (input_string[input_string.length() - 1] == ' ') {
        input_string.pop_back();
    }

    vector<string> splits;
    char delimiter = ' ';

    size_t i = 0;
    size_t pos = input_string.find(delimiter);

    while (pos != string::npos) {
        splits.push_back(input_string.substr(i, pos - i));

        i = pos + 1;
        pos = input_string.find(delimiter, i);
    }

    splits.push_back(input_string.substr(i, min(pos, input_string.length()) - i + 1));

    return splits;
}
