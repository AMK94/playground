#include <bits/stdc++.h>
#include <fstream>
using namespace std;

#include <bits/stdc++.h>

using namespace std;

void clearResetSubStrings (string & subA, string & subB, uint8_t & aSubAdd, uint8_t & bSubAdd, uint64_t & offset)
{
  subA.clear();
  subB.clear();
  aSubAdd = 1;
  bSubAdd = 1;
  offset = 0;
}

// Complete the morganAndString function below.
string morganAndString(string a, string b) {
    
    uint64_t aIndex = 0, bIndex = 0;
    uint64_t aSize = a.size(), bSize = b.size();
    uint64_t resultSize = aSize + bSize;
    uint64_t offset = 0;

    char aVal = 0, bVal = 0, initMatch = 0, temp;
    uint8_t aSubAdd = 1, bSubAdd = 1;

    string result = "";
    string subA = "", subB = "";
  
  do
  {
    if ((aIndex + offset) < aSize)
    {
      aVal = a[aIndex + offset];
    }
    else
    {
      if (aIndex == aSize)
        aVal = 0x7f;
      else
        aVal = b[bIndex];
    }
    if ((bIndex + offset) < bSize)
    {

      bVal = b[bIndex + offset];
    }
    else
    {
      if (bIndex == bSize)
        bVal = 0x7f;
      else
        bVal = a[aIndex];
    }
   
    
    if (aVal == bVal)
    {
      if (offset != 0)
      {

        if ((subA[offset] < initMatch) && aSubAdd == 1)
        {
          subA += aVal;
        }
        else if ((subA[offset] < initMatch) && aSubAdd == 1)
          aSubAdd = offset;
        
        if ((subB[offset] < initMatch) && bSubAdd == 1)
        {

          subB += bVal;

        }
        else if ((subB[offset] < initMatch) && bSubAdd == 1)
          bSubAdd = offset;
      }
      else
      {
        initMatch = aVal;
        subA += aVal;
        subB += bVal;
      }
      offset++;
    }
    else
    {
      subA += aVal;
      subB += bVal;
    }
    
    if (((offset + aIndex) == (aSize)) && ((offset + bIndex) == (bSize)))
    {
        /*
        if (aSubAdd <= bSubAdd)
        {
            result += subA;
            aIndex += subA.size();
        }
        else
        {
            result += subB;
            bIndex += subB.size();
        }
        */
        result += a[aIndex++];
            
      clearResetSubStrings (subA, subB, aSubAdd, bSubAdd, offset);

    }
    else if(aVal < bVal)
    {

      if (offset)
        subA.pop_back();
      result += subA;
      aIndex += subA.size();
      clearResetSubStrings (subA, subB, aSubAdd, bSubAdd, offset);

    }
    else if (bVal < aVal)
    {

      if (offset)
        subB.pop_back();
      result += subB;
      bIndex += subB.size();
      clearResetSubStrings (subA, subB, aSubAdd, bSubAdd, offset);
    }

    

  }while ((aIndex + bIndex) != resultSize);
    return result;
}


int main(int numArguments, char *inputNum[])
{
    ofstream outputAll;
    ifstream inputFile;
    int t;
    
    outputAll.open("finalResult.txt");

    
    if (numArguments > 1)
    {
      inputFile.open(inputNum[1]);
      inputFile >> t;
      inputFile.ignore(numeric_limits<streamsize>::max(), '\n');
      
      for (int t_itr = 0; t_itr < t; t_itr++) {
        string a;
        getline(inputFile, a);

        string b;
        getline(inputFile, b);
        
        
        string result = morganAndString(a, b);

        outputAll << result << "\n";
        cout << result << endl;
      }
    }
    else {

      cout << "Please enter the number of pairs to be used: ";
      inputFile >> t;
      inputFile.ignore(numeric_limits<streamsize>::max(), '\n');

      cout << "Please enter the string pairs belows:\n";
      for (int t_itr = 0; t_itr < t; t_itr++) {
          string a;
          getline(inputFile, a);

          string b;
          getline(inputFile, b);
          
          
          string result = morganAndString(a, b);

          outputAll << result << "\n";
          cout << result << endl;
      }
    }

    outputAll.close();

    return 0;
}
