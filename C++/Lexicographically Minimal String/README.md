#Name: Lexicographically Minimal String


##Problem: Refer to the following website for the problem prompt https://www.hackerrank.com/challenges/morgan-and-a-string/problem
           
      
The program will take a pair of strings, and arrange them in lexicographically minimal string.

The user can prepare an input file before hand and indicate it on the cmd line when calling the executable, or opt to manually enter
the parameters. The input file must adhere the rules outlined in the problem statement, at the website above.




Updated 1: The current code only clears 5/18 test cases provided on the hacker rank website. I will update it to pass all cases soon.
After some research, it seems that my program can correctly pass all edge cases. However, the run time is greater than the
correct solution. I must reduce the time complexity of the code, and it should work just fine.

Update 2: The final code passed 8/18 tests. The code will definitly provide an answer for all inputs; however the time complexity is
          stopping it from achieving a pass on the remaining tests. I manually tested input files to see if the correct output was being
          generated from the input, and after x amount of elapsed time it was. I will look into how to use suffix arrays to make the time
          complexity of this problem O(n) in the future.

          
Last Updated: Jul 13, 2018
