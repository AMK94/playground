Name: Arsalan Khan
Purpose: The purpose of this repository is to provide insight on how I code, document my code, and to let
         different individuals see the projects that I have worked on and am currently working on.

		 
Folders:
     - C: Contains simple C projects
	 - C++: Contains simple C++ projects
	 - C#: Currently only contains WPF appliactions developed
	 - Embedded: Contains projects developed for the ARM M3/M4 (Project names will indicate baremetal or RTOS)
	 - QT: Contains projects for learning the QT framework
	 - Libraries: Contains libraries for generic items (i.e. Quaternion)

Last Updated: Jul 09, 2018
